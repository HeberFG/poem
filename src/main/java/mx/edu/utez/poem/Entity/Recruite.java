package mx.edu.utez.poem.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "recruites")
public class Recruite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrecruite")
    private Long idrecruite;

    @Column(name = "position")
    private String position;

    @OneToOne
    @JoinColumn(name = "iduser", nullable = false)
    private User user;

    public Recruite() {
    }

    public Long getIdrecruite() {
        return idrecruite;
    }

    public void setIdrecruite(Long idrecruite) {
        this.idrecruite = idrecruite;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}