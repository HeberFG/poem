package mx.edu.utez.poem.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "languajes_candidates")
public class Languages_Candidates {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idlanguagecandidate")
    private Long idLanguageCandidate;

    @ManyToOne
    @JoinColumn(name = "idlanguage", nullable= true)
    private Language language;

    @ManyToOne
    @JoinColumn(name = "idcandidate", nullable = true)
    private Candidate candidate;

    @Column(name = "level")
    private String level;

    public Long getIdLanguageCandidate() {
        return idLanguageCandidate;
    }

    public void setIdLanguageCandidate(Long idLanguageCandidate) {
        this.idLanguageCandidate = idLanguageCandidate;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}