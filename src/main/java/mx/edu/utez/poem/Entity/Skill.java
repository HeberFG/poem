package mx.edu.utez.poem.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "skills")
public class Skill {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idskill")
    private Long idskill;
	
	@Column(name = "name")
    private String name;
	
	@ManyToOne
	@JoinColumn(name = "idcandidate")
	private Candidate candidate;

	public Long getIdskill() {
		return idskill;
	}

	public void setIdskill(Long idskill) {
		this.idskill = idskill;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}
}