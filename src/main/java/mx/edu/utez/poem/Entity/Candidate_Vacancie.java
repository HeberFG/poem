package mx.edu.utez.poem.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "candidates_vacancies")
public class Candidate_Vacancie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcandidatevacancie")
    private Long idCandidateVacancie;

    @ManyToOne
    @JoinColumn(name = "idcandidate")
    private Candidate candidate;
    
    @ManyToOne
    @JoinColumn(name = "idvacancies")
    private Vacant vacant;

    @Column(name = "status")
    private String status;

    public Long getIdCandidateVacancie() {
        return idCandidateVacancie;
    }

    public void setIdCandidateVacancie(Long idCandidateVacancie) {
        this.idCandidateVacancie = idCandidateVacancie;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Vacant getVacant() {
        return vacant;
    }

    public void setVacant(Vacant vacant) {
        this.vacant = vacant;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}