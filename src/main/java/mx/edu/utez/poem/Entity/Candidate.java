package mx.edu.utez.poem.Entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "candidates")
public class Candidate {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcandidate")
    private Long idcandidate;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "cvdescription", nullable = false)
    private String cvdescription;

    @OneToOne
    @JoinColumn(name = "iduser", nullable = false)
    private User user;
    
    @OneToMany(mappedBy = "candidate")
    private Set<Languages_Candidates> languagesCandidates;
    
    public Candidate() {}

    public Long getIdcandidate() {
        return idcandidate;
    }

    public void setIdcandidates(Long idcandidate) {
        this.idcandidate = idcandidate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCvdescription() {
        return cvdescription;
    }

    public void setCvdescription(String cvdescription) {
        this.cvdescription = cvdescription;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Languages_Candidates> getLanguagesCandidates() {
        return languagesCandidates;
    }

    public void setLanguagesCandidates(Set<Languages_Candidates> languagesCandidates) {
        this.languagesCandidates = languagesCandidates;
    }
}
