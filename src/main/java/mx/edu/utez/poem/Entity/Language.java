package mx.edu.utez.poem.Entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "languages")
public class Language {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idlanguage")
    private Long idlanguage;
	
	@Column(name = "name")
    private String name;

	@OneToMany(mappedBy = "language")
	private Set<Languages_Candidates> languagesCandidates;

	public Long getIdlanguage() {
		return idlanguage;
	}

	public void setIdlanguage(Long idlanguage) {
		this.idlanguage = idlanguage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Languages_Candidates> getLanguagesCandidates() {
		return languagesCandidates;
	}

	public void setLanguagesCandidates(Set<Languages_Candidates> languagesCandidates) {
		this.languagesCandidates = languagesCandidates;
	}
	
	

}
