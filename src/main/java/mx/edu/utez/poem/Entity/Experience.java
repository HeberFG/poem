package mx.edu.utez.poem.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "experiences")
public class Experience {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idexperience")
    private Long idexperience;
	
    @Column(name = "position")
    private String position;
    
    @Column(name = "startyear")
    private int startyear;
    
    @Column(name = "endyear")
    private int endyear;
    
    @Column(name = "activities")
    private String activities;
    
    @ManyToOne
    @JoinColumn(name = "idcandidate")
    private Candidate candidate;

	public Long getIdexperience() {
		return idexperience;
	}

	public void setIdexperience(Long idexperience) {
		this.idexperience = idexperience;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getStartyear() {
		return startyear;
	}

	public void setStartyear(int startyear) {
		this.startyear = startyear;
	}

	public int getEndyear() {
		return endyear;
	}

	public void setEndyear(int endyear) {
		this.endyear = endyear;
	}

	public String getActivities() {
		return activities;
	}

	public void setActivities(String activities) {
		this.activities = activities;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}
    
    

}
