package mx.edu.utez.poem.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "courses")
public class Course {

	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcours")
    private Long idcours;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "nohoras")
    private int nohoras;
	
	@Column(name = "institutionname")
    private String institutionname;
	
	@Column(name = "realizationyear")
    private int realizationyear;
	
	@ManyToOne
	@JoinColumn(name = "idcandidate")
	private Candidate candidate;

	public Long getIdcours() {
		return idcours;
	}

	public void setIdcours(Long idcours) {
		this.idcours = idcours;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNohoras() {
		return nohoras;
	}

	public void setNohoras(int nohoras) {
		this.nohoras = nohoras;
	}

	public String getInstitutionname() {
		return institutionname;
	}

	public void setInstitutionname(String institutionname) {
		this.institutionname = institutionname;
	}

	public int getRealizationyear() {
		return realizationyear;
	}

	public void setRealizationyear(int realizationyear) {
		this.realizationyear = realizationyear;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}
}