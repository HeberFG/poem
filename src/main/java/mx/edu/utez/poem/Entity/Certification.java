package mx.edu.utez.poem.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "certifications")
public class Certification {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcertification")
    private Long idcertification;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "company")
    private String company;
	
	@Column(name = "obtainingdate")
	@DateTimeFormat(pattern="dd/MM/yyyy")
    private Date obtainingdate;
	
	@Column(name = "expiredate")
	@DateTimeFormat(pattern="dd/MM/yyyy")
    private Date expiredate;
	
	 @ManyToOne
	 @JoinColumn(name = "idcandidate")
	 private Candidate candidate;

	public Long getIdcertification() {
		return idcertification;
	}

	public void setIdcertifications(Long idcertification) {
		this.idcertification = idcertification;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getObtainingdate() {
		return obtainingdate;
	}

	public void setObtainingdate(Date obtainingdate) {
		this.obtainingdate = obtainingdate;
	}

	public Date getExpiredate() {
		return expiredate;
	}

	public void setExpiredate(Date expiredate) {
		this.expiredate = expiredate;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}
}