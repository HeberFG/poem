package mx.edu.utez.poem.Entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "vacancies")
public class Vacant {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idvacant")
    private Long idvacant;
	
	@Column(name = "title")
    private String title;
	
	@Column(name = "description")
    private String description;
	
	@Column(name = "modality")
    private String modality;
	
	@Column(name = "horary")
    private String horary;
	
	@Column(name = "startdate")
	@DateTimeFormat(pattern="dd/MM/yyyy")
    private Date startdate;
	
	@Column(name = "expiredate")
	@DateTimeFormat(pattern="dd/MM/yyyy")
    private Date expiredate;
	
	@Column(name = "salary")
    private String salary;
	
	@Column(name = "paymentperiod")
    private String paymentperiod;
	
	@Column(name = "enabled")
    private Boolean enabled;
	
	@Column(name = "stage")
	private String stage; //proceso de la vacante: inicio, en proceso, termino
	
	@ManyToOne
	@JoinColumn(name = "idrecruites")
	private Recruite recruites;
	
	@ManyToOne
    @JoinColumn(name = "idstate")
    private State state;

	@ManyToMany
    @JoinTable(name = "benefits_vacancies", joinColumns = @JoinColumn(name = "idvacancies"), inverseJoinColumns = @JoinColumn(name = "idbenefits"))
    private Set<Benefit> benefits = new HashSet<>();
	
	public Long getIdvacant() {
		return idvacant;
	}

	public void setIdvacant(Long idvacant) {
		this.idvacant = idvacant;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getHorary() {
		return horary;
	}

	public void setHorary(String horary) {
		this.horary = horary;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getExpiredate() {
		return expiredate;
	}

	public void setExpiredate(Date expiredate) {
		this.expiredate = expiredate;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getPaymentperiod() {
		return paymentperiod;
	}

	public void setPaymentperiod(String paymentperiod) {
		this.paymentperiod = paymentperiod;
	}

	public Recruite getRecruites() {
		return recruites;
	}

	public void setRecruites(Recruite recruites) {
		this.recruites = recruites;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	
	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public Set<Benefit> getBenefits() {
		return benefits;
	}

	public void setBenefits(Set<Benefit> benefits) {
		this.benefits = benefits;
	}
}