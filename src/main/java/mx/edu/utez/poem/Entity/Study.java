package mx.edu.utez.poem.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "studies")
public class Study {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idstudy")
    private Long idstudy;
	
	@Column(name = "collegename")
    private String collegename;

	@Column(name = "career")
    private String career;
	
	@Column(name = "degree")
    private String degree;
	
	@Column(name = "startyear")
    private int startyear;
	
	@Column(name = "endyear")
    private int endyear;
	
	@ManyToOne
	@JoinColumn(name = "idcandidate")
	private Candidate idcandidate;

	public Long getIdstudy() {
		return idstudy;
	}

	public void setIdstudy(Long idstudy) {
		this.idstudy = idstudy;
	}

	public String getCollegename() {
		return collegename;
	}

	public void setCollegename(String collegename) {
		this.collegename = collegename;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public int getStartyear() {
		return startyear;
	}

	public void setStartyear(int startyear) {
		this.startyear = startyear;
	}

	public int getEndyear() {
		return endyear;
	}

	public void setEndyear(int endyear) {
		this.endyear = endyear;
	}

	public Candidate getIdcandidate() {
		return idcandidate;
	}

	public void setIdcandidate(Candidate idcandidate) {
		this.idcandidate = idcandidate;
	}
}