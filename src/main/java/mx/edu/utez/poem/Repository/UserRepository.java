package mx.edu.utez.poem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.poem.Entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
    
}
