package mx.edu.utez.poem.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Certification;

@Repository
public interface CertificationRepository extends JpaRepository<Certification, Long> {

    List<Certification> findByCandidate(Candidate candidate);
}
