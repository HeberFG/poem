package mx.edu.utez.poem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.poem.Entity.Candidate_Vacancie;

@Repository
public interface CandidateVacancieRepository extends JpaRepository<Candidate_Vacancie, Long> {

}
