package mx.edu.utez.poem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.User;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long>{

    Candidate findByUser(User user);
}
