package mx.edu.utez.poem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.poem.Entity.Languages_Candidates;

@Repository
public interface LanguageCandidateRepository extends JpaRepository<Languages_Candidates, Long>{
    
}
