package mx.edu.utez.poem.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Study;

@Repository
public interface StudyRepository extends JpaRepository<Study, Long> {

    List<Study> findByIdcandidate(Candidate candidate);
}
