package mx.edu.utez.poem.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.edu.utez.poem.Entity.Vacant;

@Repository
public interface VacantRepository extends JpaRepository<Vacant, Long> {

	
	@Query(value = "SELECT * FROM vacancies v WHERE v.title=:title", nativeQuery=true)
    List<Vacant>  findBytitle(@Param("title") String title);
	
	@Query(value = "SELECT * FROM vacancies v JOIN states s ON v.idstate=s.idstate WHERE s.name=:name", nativeQuery=true)
    List<Vacant>  findByname(@Param("name") String name);
	
}
