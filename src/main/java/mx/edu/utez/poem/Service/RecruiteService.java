package mx.edu.utez.poem.Service;

import mx.edu.utez.poem.Entity.Recruite;

public interface RecruiteService {

    Recruite findRecruiteById(Long id);

    Boolean saveRecruite(Recruite recruite);

    Boolean updateRecruite(Recruite recruite);

    Boolean deleteRecruite(Long id);
}
