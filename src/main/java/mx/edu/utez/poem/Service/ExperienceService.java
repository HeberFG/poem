package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Experience;

public interface ExperienceService {
	List<Experience> listExperiences();

	List<Experience> findByCandidate(Candidate candidate);

    Boolean saveExperience(Experience experience);

    Boolean updateExperience(Experience experience);

    Boolean deleteExperience(Long id);
}
