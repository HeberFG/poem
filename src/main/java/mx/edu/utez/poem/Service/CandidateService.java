package mx.edu.utez.poem.Service;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.User;

public interface CandidateService {

    Candidate findCandidateById(Long id);

    Boolean saveCandidate(Candidate candidate);

    Boolean updateCandidate(Candidate candidate);

    Boolean deleteCandidate(Long id);

    Candidate findByUser(User user);
}
