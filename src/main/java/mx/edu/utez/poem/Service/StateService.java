package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.State;

public interface StateService {
    List<State> listStates();
}
