package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.User;


public interface UserService {
    List<User> listUsers();

    User findUserByUsername(String username);

    User saveUser(User user);

    Boolean updateUser(User user);

    Boolean deleteUser(String username);
}
