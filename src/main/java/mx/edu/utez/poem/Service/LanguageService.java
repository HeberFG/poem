package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Language;

public interface LanguageService {
	List<Language> listLanguages();
}
