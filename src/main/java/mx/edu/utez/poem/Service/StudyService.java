package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Study;

public interface StudyService {
	List<Study> listStudies();

	List<Study> findByIdcandidate(Candidate candidate);

    Boolean saveStudy(Study study);

    Boolean updateStudy(Study study);

    Boolean deleteStudy(Long id);
}
