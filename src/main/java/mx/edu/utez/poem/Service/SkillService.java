package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Skill;

public interface SkillService {
	List<Skill> listSkills();

	List<Skill> findByCandidate(Candidate candidate);

    Boolean saveSkill(Skill skill);

    Boolean updateSkill(Skill skill);

    Boolean deleteSkill(Long id);
}
