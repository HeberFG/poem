package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Rol;

public interface RolService {
    List <Rol> listRols();
}
