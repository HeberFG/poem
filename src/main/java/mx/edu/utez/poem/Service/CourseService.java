package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Course;

public interface CourseService {
	List<Course> listCours();

    Boolean saveCours(Course course);

    Boolean updateCours(Course course);

    Boolean deleteCours(Long id);

    List<Course> findByIdcandidate(Candidate candidate);
}
