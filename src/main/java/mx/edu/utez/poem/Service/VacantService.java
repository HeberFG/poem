package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Vacant;

public interface VacantService {
	List<Vacant> listVacants();

    Boolean saveVacant(Vacant vacant);
    
    List<Vacant> findByTitle(String title);

    List<Vacant> findByName(String name);
    
    Boolean updateVacant(Vacant vacant);
    
    Vacant showVacant(long idvacant);
    
    Boolean deleteVacant(Long id);
}
