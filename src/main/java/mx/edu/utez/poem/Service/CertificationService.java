package mx.edu.utez.poem.Service;

import java.util.List;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Certification;

public interface CertificationService {
	List<Certification> listCertifications();

	List<Certification> findByCandidate(Candidate candidate);

    Boolean saveCertification(Certification certification);

    Boolean updateCertification(Certification certification);

    Boolean deleteCertification(Long id);
}
