package mx.edu.utez.poem.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Vacant;
import mx.edu.utez.poem.Repository.VacantRepository;
import mx.edu.utez.poem.Service.VacantService;

@Service
public class VacantServiceImpl implements VacantService{
	
	@Autowired
	VacantRepository vacantRepository;

	public List<Vacant> listVacants() {
		return vacantRepository.findAll();
	}
	
	public List<Vacant> findByTitle(String title) {
		return vacantRepository.findBytitle(title);
	}
	
	public List<Vacant> findByName(String name) {
		return vacantRepository.findByname(name);
	}

	public Boolean saveVacant(Vacant vacant) {
		Boolean response = false;
		Vacant coursSaved = vacantRepository.save(vacant);
		if(coursSaved != null) {
			response = true;
		}
		return response;
	}

	public Boolean updateVacant(Vacant vacant) {
		Boolean response = false;
		if(vacantRepository.findById(vacant.getIdvacant()) != null) {
			if(vacantRepository.save(vacant) != null ) {
				response = true;
			}
		}
		return response;
	}
	
	
	public Vacant showVacant(long idvacant) {
		Optional<Vacant> optional = vacantRepository.findById(idvacant);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	public Boolean deleteVacant(Long id) {
		Boolean response = false;
		if (vacantRepository.findById(id) != null) {
			vacantRepository.deleteById(id);
            response = true;
        }
        return response;
	}
	
	
}
