package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Rol;
import mx.edu.utez.poem.Repository.RolRepository;
import mx.edu.utez.poem.Service.RolService;

@Service
public class RolServiceImpl implements RolService{

    @Autowired
    RolRepository rolRepository;

    @Override
    public List<Rol> listRols() {
        return rolRepository.findAll();
    }
}
