package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Language;
import mx.edu.utez.poem.Repository.LanguageRepository;
import mx.edu.utez.poem.Service.LanguageService;

@Service
public class LanguageServiceImpl implements LanguageService{
	
	@Autowired
	LanguageRepository languageRepository;

	public List<Language> listLanguages() {
		return languageRepository.findAll();
	}
}
