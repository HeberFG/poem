package mx.edu.utez.poem.ServiceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.User;
import mx.edu.utez.poem.Repository.CandidateRepository;
import mx.edu.utez.poem.Service.CandidateService;

@Service
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    CandidateRepository candidateRepository;

    @Override
    public Candidate findCandidateById(Long id) {
        Optional <Candidate> candidate = candidateRepository.findById(id);
        if (candidate.isPresent()) {
            return candidate.get();
        }
        return null;
    }

    @Override
    public Boolean saveCandidate(Candidate candidate) {
        Boolean response = false;
        Candidate candidate2 = candidateRepository.save(candidate);
        if (candidate2 != null) {
            response = true;
        }
        return response;
    }

    @Override
    public Boolean updateCandidate(Candidate candidate) {
        Boolean response = false;
        Candidate candidate2 = candidateRepository.save(candidate);
        if (candidate2 != null) {
            response = true;
        }
        return response;
    }

    @Override
    public Boolean deleteCandidate(Long id) {
        Boolean response = false;
        if (candidateRepository.findById(id).isPresent()) {
            candidateRepository.deleteById(id);
            response = true;
        }
        return response;
    }

    @Override
    public Candidate findByUser(User user) {
        return candidateRepository.findByUser(user);
    }
}