package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Experience;
import mx.edu.utez.poem.Repository.ExperienceRepository;
import mx.edu.utez.poem.Service.ExperienceService;

@Service
public class ExperienceServiceImpl implements ExperienceService {

	@Autowired
	ExperienceRepository experienceRepository;

	public List<Experience> listExperiences() {
		return experienceRepository.findAll();
	}

	public Boolean saveExperience(Experience experience) {
		Boolean response = false;
		if (experienceRepository.save(experience) != null) {
			response = true;
		}
		return response;
	}

	public Boolean updateExperience(Experience experience) {
		Boolean response = false;
		if (experienceRepository.findById(experience.getIdexperience()) != null) {
			if (experienceRepository.save(experience) != null) {
				response = true;
			}
		}
		return response;
	}

	public Boolean deleteExperience(Long id) {
		Boolean response = false;
		if (experienceRepository.findById(id) != null) {
			experienceRepository.deleteById(id);
			response = true;
		}
		return response;
	}

	@Override
	public List<Experience> findByCandidate(Candidate candidate) {
		return experienceRepository.findByCandidate(candidate);
	}

}
