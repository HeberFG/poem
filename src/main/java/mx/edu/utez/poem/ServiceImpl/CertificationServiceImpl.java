package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Certification;
import mx.edu.utez.poem.Repository.CertificationRepository;
import mx.edu.utez.poem.Service.CertificationService;

@Service
public class CertificationServiceImpl implements CertificationService {
	
	@Autowired
	CertificationRepository certificationRepository;
	

	public List<Certification> listCertifications() {
		return certificationRepository.findAll();
	}

	public Boolean saveCertification(Certification certification) {
		Boolean response = false;
		Certification certificationSaved = certificationRepository.save(certification);
		if(certificationSaved != null) {
			response = true;
		}
		return response;
	}

	public Boolean updateCertification(Certification certification) {
		Boolean response = false;
		if(certificationRepository.findById(certification.getIdcertification()) != null) {
			if(certificationRepository.save(certification) != null ) {
				response = true;
			}
		}
		return response;
	}

	public Boolean deleteCertification(Long id) {
		Boolean response = false;
		if (certificationRepository.findById(id) != null) {
			certificationRepository.deleteById(id);
            response = true;
        }
        return response;
	}

	@Override
	public List<Certification> findByCandidate(Candidate candidate) {
		return certificationRepository.findByCandidate(candidate);
	}


}
