package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Course;
import mx.edu.utez.poem.Repository.CourseRepository;
import mx.edu.utez.poem.Service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {
	
	@Autowired
	CourseRepository courseRepository;

	public List<Course> listCours() {
		return courseRepository.findAll();
	}

	public Boolean saveCours(Course course) {
		Boolean response = false;
		Course coursSaved = courseRepository.save(course);
		if(coursSaved != null) {
			response = true;
		}
		return response;
	}

	public Boolean updateCours(Course course) {
		Boolean response = false;
		if(courseRepository.findById(course.getIdcours()) != null) {
			if(courseRepository.save(course) != null ) {
				response = true;
			}
		}
		return response;
	}

	public Boolean deleteCours(Long id) {
		Boolean response = false;
		if (courseRepository.findById(id) != null) {
			courseRepository.deleteById(id);
            response = true;
        }
        return response;
	}

	@Override
	public List<Course> findByIdcandidate(Candidate candidate) {
		return courseRepository.findByCandidate(candidate);
	}
}
