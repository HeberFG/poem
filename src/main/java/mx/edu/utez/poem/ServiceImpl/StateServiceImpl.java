package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.State;
import mx.edu.utez.poem.Repository.StateRepository;
import mx.edu.utez.poem.Service.StateService;

@Service
public class StateServiceImpl implements StateService{

    @Autowired StateRepository stateRepository;

    @Override
    public List<State> listStates() {
        return stateRepository.findAll();
    }


}
