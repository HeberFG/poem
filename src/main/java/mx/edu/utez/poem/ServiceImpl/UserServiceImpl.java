package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.WebSecurityConfig;
import mx.edu.utez.poem.Entity.User;
import mx.edu.utez.poem.Repository.UserRepository;
import mx.edu.utez.poem.Service.UserService;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    WebSecurityConfig securityConfig;

    @Override
    public List<User> listUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserByUsername(String username) {
        if (userRepository.findById(username).isPresent()) {
            return userRepository.findById(username).get();
        }
        return null;
    }

    @Override
    public User saveUser(User user) {
        user.setPassword(securityConfig.passwordEncoder().encode(user.getPassword()));
        User userSaved = userRepository.save(user);
        if(userSaved != null){
            return userSaved;
        }
        return null;
    }

    @Override
    public Boolean updateUser(User user) {
        Boolean response = false;
        if (userRepository.findById(user.getUsername()) != null) {
            if(userRepository.save(user) != null){
                response = true;
            }
        }
        return response;
    }

    @Override
    public Boolean deleteUser(String username) {
        Boolean response = false;
        if (userRepository.findById(username) != null) {
            userRepository.deleteById(username);
            response = true;
        }
        return response;
    }
}