package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Skill;
import mx.edu.utez.poem.Repository.SkillRepository;
import mx.edu.utez.poem.Service.SkillService;

@Service
public class SkillServiceImpl implements SkillService {

	@Autowired
	SkillRepository skillRepository;

	public List<Skill> listSkills() {
		return skillRepository.findAll();
	}

	public Boolean saveSkill(Skill skill) {
		Boolean response = false;
		if (skillRepository.save(skill) != null) {
			response = true;
		}
		return response;
	}

	public Boolean updateSkill(Skill skill) {
		Boolean response = false;
		if (skillRepository.findById(skill.getIdskill()) != null) {
			if (skillRepository.save(skill) != null) {
				response = true;
			}
		}
		return response;
	}

	public Boolean deleteSkill(Long id) {
		Boolean response = false;
		if (skillRepository.findById(id) != null) {
			skillRepository.deleteById(id);
			response = true;
		}
		return response;
	}

	@Override
	public List<Skill> findByCandidate(Candidate candidate) {
		return skillRepository.findByCandidate(candidate);
	}

}
