package mx.edu.utez.poem.ServiceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Recruite;
import mx.edu.utez.poem.Repository.RecruiteRepository;
import mx.edu.utez.poem.Service.RecruiteService;

@Service
public class RecruiteServiceImpl implements RecruiteService {

    @Autowired
    RecruiteRepository recruiteRepository;

    @Override
    public Recruite findRecruiteById(Long id) {
        Optional<Recruite> recruite = recruiteRepository.findById(id);
        if (recruite.isPresent()) {
            return recruite.get();
        }
        return null;
    }

    @Override
    public Boolean saveRecruite(Recruite recruite) {
        Boolean response = false;
        Recruite recruite2 = recruiteRepository.save(recruite);
        if (recruite2 != null) {
            response = true;
        }
        return response;
    }

    @Override
    public Boolean updateRecruite(Recruite recruite) {
        Boolean response = false;
        Recruite recruite2 = recruiteRepository.save(recruite);
        if (recruite2 != null) {
            response = true;
        }
        return response;
    }

    @Override
    public Boolean deleteRecruite(Long id) {
        Boolean response = false;
        if (recruiteRepository.findById(id).isPresent()) {
            recruiteRepository.deleteById(id);
            response = true;
        }
        return response;
    }
}
