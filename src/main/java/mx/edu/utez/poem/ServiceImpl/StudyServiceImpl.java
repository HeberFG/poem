package mx.edu.utez.poem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Study;
import mx.edu.utez.poem.Repository.StudyRepository;
import mx.edu.utez.poem.Service.StudyService;

@Service
public class StudyServiceImpl implements StudyService {

	@Autowired
	StudyRepository studyRepository;

	public List<Study> listStudies() {
		return studyRepository.findAll();
	}

	public Boolean saveStudy(Study study) {
		Boolean response = false;
		if (studyRepository.save(study) != null) {
			response = true;
		}
		return response;
	}

	public Boolean updateStudy(Study study) {
		Boolean response = false;
		if (studyRepository.findById(study.getIdstudy()) != null) {
			if (studyRepository.save(study) != null) {
				response = true;
			}
		}
		return response;
	}

	public Boolean deleteStudy(Long id) {
		Boolean response = false;
		if (studyRepository.findById(id) != null) {
			studyRepository.deleteById(id);
			response = true;
		}
		return response;
	}

	@Override
	public List<Study> findByIdcandidate(Candidate candidate) {
		return studyRepository.findByIdcandidate(candidate);
	}
}