package mx.edu.utez.poem.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.utez.poem.Entity.Languages_Candidates;
import mx.edu.utez.poem.Repository.LanguageCandidateRepository;
import mx.edu.utez.poem.Service.LanguageCandidateService;

@Service
public class LanguageCandidateServiceImpl implements LanguageCandidateService{

    @Autowired
    LanguageCandidateRepository languageCandidateRepository;

    @Override
    public Boolean saveLanguaje(Languages_Candidates languages_Candidates) {
        Boolean response = false;
        if (languageCandidateRepository.save(languages_Candidates) != null) {
            response = true;
        }
        return response;
    }
}