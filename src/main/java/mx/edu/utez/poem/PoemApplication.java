package mx.edu.utez.poem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class PoemApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoemApplication.class, args);
	}

}
