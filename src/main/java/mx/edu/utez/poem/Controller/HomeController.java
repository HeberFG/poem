package mx.edu.utez.poem.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.edu.utez.poem.ServiceImpl.UserServiceImpl;

@Controller
@RequestMapping()
public class HomeController {

    @Autowired
    UserServiceImpl userImpl;
    
    @GetMapping("/")
    public String getHome(Authentication authentication){
        String view = "index";
        if (authentication != null) {
            String rol = authentication.getAuthorities().toString().replace("[", "").replace("]", "");
            System.out.println(rol);
            switch (rol) {
                case "ROLE_ADMIN":
                    view = "homeAdmin";
                    break;
                    case "ROLE_CANDIDATE":
                    view = "homeCandidate";
                    break;
                    case "ROLE_RECRUITER":
                    view = "homeRecruiter";
                    break;
                default:
                    view = "index";
                    break;
            }
        }
        return view;
    }
}
