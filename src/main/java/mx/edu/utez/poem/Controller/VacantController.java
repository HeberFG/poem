package mx.edu.utez.poem.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import mx.edu.utez.poem.Entity.Vacant;
import mx.edu.utez.poem.ServiceImpl.VacantServiceImpl;

@Controller
@RequestMapping(value = "/poem/vacancies")
public class VacantController {

	
	
	@Autowired
	private VacantServiceImpl vacantServiceImpl;
	
	
	//Listar Vacantes GET
	@GetMapping(value =  "/list")
	public String listVacancies(Model model) {
	   List<Vacant> listVacants = vacantServiceImpl.listVacants();
	   model.addAttribute("listVacants",listVacants );
	   return "vacancies/listVacants";
	}
	
	//Registrar Vacante GET
	@GetMapping("/create")
	public String createVacant(Vacant vacant, Model modelo) {
		return "vacants/formVacant";
	}
	
	//Guardar Vacante POST
	@PostMapping("/save")
	public String saveVacant(Vacant vacant, Model modelo, RedirectAttributes redirectAttributes) {
		vacant.setStage("Inicio");
		boolean respuesta = vacantServiceImpl.saveVacant(vacant);
		if (respuesta) {
			redirectAttributes.addFlashAttribute("msg_success", "Registro Exitoso");
			return "redirect:/vacancies/list";
		}else {
			redirectAttributes.addFlashAttribute("msg_error", "Registro Fallido por favor intente de nuevo");
			return "redirect:/vacancies/create";
		}
	}
	
	//Editar Vacante GET
	@GetMapping("/update")
	public String updateVacant(Vacant vacant, Model modelo) {
		return "vacants/formEditVacant";
	}
	
	//Guardar Vagante PUT
	@PutMapping("/edit")
	public String editVacant(Vacant vacant, Model modelo, RedirectAttributes redirectAttributes) {
		
			Vacant vacantExist = vacantServiceImpl.showVacant(vacant.getIdvacant());
			vacant.setTitle(vacantExist.getTitle());
			vacant.setDescription(vacantExist.getDescription());
			vacant.setModality(vacantExist.getModality());
			vacant.setHorary(vacantExist.getHorary());
			vacant.setStartdate(vacantExist.getStartdate());
			vacant.setExpiredate(vacantExist.getExpiredate());
			vacant.setSalary(vacantExist.getSalary());
			vacant.setPaymentperiod(vacantExist.getPaymentperiod());
			vacant.setRecruites(vacantExist.getRecruites());
			vacant.setState(vacantExist.getState());
		
		boolean respuesta = vacantServiceImpl.updateVacant(vacant);
		if (respuesta) {
			redirectAttributes.addFlashAttribute("msg_success", "Modificación Exitosa");
			return "redirect:/vacancies/list";
		}else {
			redirectAttributes.addFlashAttribute("msg_error", "Modificación Fallida por favor intente de nuevo");
			return "redirect:/vacancies/update";
		}
	}
	
	
	//Eliminar vacante
	@GetMapping("/delete/{idvacant}")
	public String deleteVacant(@PathVariable long idvacant, RedirectAttributes redirectAttributes) {
		boolean respuesta = vacantServiceImpl.deleteVacant(idvacant);
		if (respuesta) {
			redirectAttributes.addFlashAttribute("msg_success", "Registro Eliminado");
			
		}else {
			redirectAttributes.addFlashAttribute("msg_error", "Eliminacion Fallida");
			
		}
		return "redirect:/vacancies/list";
	}
	
	
	//Detalle de vacante GET
	@GetMapping("/detail/{idvacant}")
	public String detailVacant(@PathVariable long idvacant, Model modelo, RedirectAttributes redirectAttributes) {
		Vacant vacant = vacantServiceImpl.showVacant(idvacant);
		if (vacant != null) {
			modelo.addAttribute("vacant",vacant);
			
			return "vacancies/detailVacant";
		}
		redirectAttributes.addFlashAttribute("msg_error", "Registro No Encontrado");
		return "redirect:/vacancies/list";
	}
	
	
	//Buscar por Puesto GET
	@GetMapping(value =  "/listByTitle/{title}")
	public String listVacanciesByTitle(@PathVariable String title, Model model) {
	   List<Vacant> listVacantsByTitle = vacantServiceImpl.findByTitle(title);
	   model.addAttribute("listVacantsByTitle",listVacantsByTitle );
	   return "vacants/listVacants";
	}
	
	
	
	//Buscar por Estado de la republica GET
	@GetMapping(value =  "/listByName/{name}")
	public String listVacanciesByName(@PathVariable String name, Model model) {
	   List<Vacant> listVacantsByName = vacantServiceImpl.findByName(name);
	   model.addAttribute("listVacantsByName",listVacantsByName );
	   return "vacants/listVacants";
	}
	
	
	//Deshabilitar Vacante
	@GetMapping("/deleteStatus/{idvacant}")
    public String deleteLogical(@PathVariable long idvacant, RedirectAttributes redirectAttributes) {
        Vacant vacant = vacantServiceImpl.showVacant(idvacant);
        if (vacant != null) {
            vacant.setEnabled(!vacant.getEnabled());
            vacantServiceImpl.saveVacant(vacant);
            redirectAttributes.addFlashAttribute("message", "¡Cambio exitoso!");
            return "redirect:/vacants/list";
        } else {
            redirectAttributes.addFlashAttribute("message_error", "¡Cambio fallido!");
            return "redirect:/vacants/list";
        }
    }
	
	
	//Cambiar estado de la Vacante GET
	@GetMapping("/changeStage")
	public String changeStageVacant(Vacant vacant, Model modelo) {
		return "vacants/formEditStageVacant";
	}
	
	
	//Cambiar estado de la Vacante GET
	@PutMapping("/editStage")
	public String editStageVacant(Vacant vacant, Model modelo, RedirectAttributes redirectAttributes) {
		
			Vacant vacantExist = vacantServiceImpl.showVacant(vacant.getIdvacant());
			vacant.setStage(vacantExist.getStage());
		
		boolean respuesta = vacantServiceImpl.updateVacant(vacant);
		if (respuesta) {
			redirectAttributes.addFlashAttribute("msg_success", "Etapa Modificada Exitosamente");
			return "redirect:/vacancies/list";
		}else {
			redirectAttributes.addFlashAttribute("msg_error", "Modificación Fallida por favor intente de nuevo");
			return "redirect:/vacancies/update";
		}
	}

	
	
	
	
	
}
