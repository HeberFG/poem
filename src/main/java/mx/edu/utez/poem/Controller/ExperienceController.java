package mx.edu.utez.poem.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.edu.utez.poem.Entity.Experience;
import mx.edu.utez.poem.ServiceImpl.ExperienceServiceImpl;

@Controller
@RequestMapping("/poem")
public class ExperienceController {
    
    @Autowired
    ExperienceServiceImpl experienceImpl;

    @PostMapping(value="/saveExperience")
    public String saveExperience(Experience experience) {
        if (experienceImpl.saveExperience(experience)) {
            return "redirect:/poem/profile";
        }
        return "redirect:/poem/profile";
    }
}