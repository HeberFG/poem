package mx.edu.utez.poem.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mx.edu.utez.poem.Entity.Languages_Candidates;
import mx.edu.utez.poem.ServiceImpl.CandidateServiceImpl;
import mx.edu.utez.poem.ServiceImpl.LanguageCandidateServiceImpl;

import org.springframework.web.bind.annotation.PostMapping;


@Controller
@RequestMapping("/poem")
public class LanguageController {

    @Autowired
    LanguageCandidateServiceImpl languageCandidateImpl;

    @Autowired
    CandidateServiceImpl candidateImpl;
    
    @PostMapping(value="/saveLanguage")
    public String saveLanguageOfUser(Languages_Candidates languagesCandidates, @RequestParam Long idCandidate) {
        languagesCandidates.setCandidate(candidateImpl.findCandidateById(idCandidate));
        if (languageCandidateImpl.saveLanguaje(languagesCandidates)) {
            return "redirect:/poem/profile";
        }
        return "redirect:/";
    }
}