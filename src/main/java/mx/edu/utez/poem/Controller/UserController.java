package mx.edu.utez.poem.Controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mx.edu.utez.poem.Entity.Candidate;
import mx.edu.utez.poem.Entity.Recruite;
import mx.edu.utez.poem.Entity.User;
import mx.edu.utez.poem.ServiceImpl.CandidateServiceImpl;
import mx.edu.utez.poem.ServiceImpl.CertificationServiceImpl;
import mx.edu.utez.poem.ServiceImpl.CourseServiceImpl;
import mx.edu.utez.poem.ServiceImpl.ExperienceServiceImpl;
import mx.edu.utez.poem.ServiceImpl.LanguageServiceImpl;
import mx.edu.utez.poem.ServiceImpl.RecruiteServiceImpl;
import mx.edu.utez.poem.ServiceImpl.RolServiceImpl;
import mx.edu.utez.poem.ServiceImpl.SkillServiceImpl;
import mx.edu.utez.poem.ServiceImpl.StateServiceImpl;
import mx.edu.utez.poem.ServiceImpl.StudyServiceImpl;
import mx.edu.utez.poem.ServiceImpl.UserServiceImpl;


@Controller
@RequestMapping("/poem")
public class UserController {

    @Autowired 
    StateServiceImpl stateImpl;

    @Autowired 
    UserServiceImpl userImpl;

    @Autowired
    RolServiceImpl rolImpl;

    @Autowired
    RecruiteServiceImpl recruiteImpl;

    @Autowired
    CandidateServiceImpl candidateImpl;

    @Autowired
    LanguageServiceImpl languageImpl;

    @Autowired
    CourseServiceImpl courseImpl;

    @Autowired
    StudyServiceImpl studyImpl;

    @Autowired
    ExperienceServiceImpl experienceImpl;

    @Autowired
    CertificationServiceImpl certificationImpl;

    @Autowired
    SkillServiceImpl skillImpl;

    //Formulario de registro de usuario general (ROLE_ADMIN)
    @GetMapping(value = "/register")
    public String register(Model model) {
        model.addAttribute("listStates", stateImpl.listStates());
        return "users/forms/formUser";
    }
    
    //Método de guardado de usuario general (ROLE_ADMIN)
    @PostMapping(value="/saveUser")
    public String saveUser(User user, @RequestParam("fecha") String fecha) throws ParseException{
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        user.setBirthDate(format.parse(fecha));
        if (userImpl.saveUser(user) != null) {
            return "redirect:/login";
        }
        return "redirect:/login";
    }

    //Formulario de registro de usuario reclutador (ROLE_RECRUITER)
    @GetMapping(value = "/registerRecruiter")
    public String registerRecruiter(Model model) {
        model.addAttribute("listStates", stateImpl.listStates());
        return "users/forms/formRecruit";
    }
    
    //Método de guardado de usuario reclutador (ROLE_RECRUITER)
    @PostMapping(value="/saveRecruiter")
    public String saveRecruiter(User user, @RequestParam("position") String position, @RequestParam("fecha") String fecha) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        user.setBirthDate(format.parse(fecha));
        User savedUser = userImpl.saveUser(user);
        if (savedUser != null) {
            Recruite recruite = new Recruite();
            recruite.setPosition(position);
            recruite.setUser(savedUser);
            if (recruiteImpl.saveRecruite(recruite)) {
                return "redirect: /login";
            }
        }
        return "redirect: /login";
    }

    //Formulario de registro de usuario general (ROLE_CANDIDATE)
    @GetMapping(value = "/registerCandidate")
    public String registerCandidate(Model model) {
        model.addAttribute("listStates", stateImpl.listStates());
        return "users/forms/formCandidate";
    }
    
    //Método de guardado de usuario general (ROLE_CANDIDATE)
    @PostMapping(value="/saveCandidate")
    public String saveCandidate(User user, @RequestParam("fecha") String fecha, @RequestParam("title") String title, @RequestParam("cvdescription") String cvdescription) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        user.setBirthDate(format.parse(fecha));
        User savedUser = userImpl.saveUser(user);
        if (savedUser != null) {
            Candidate candidate = new Candidate();
            candidate.setUser(savedUser);
            candidate.setTitle(title);
            candidate.setCvdescription(cvdescription);
            if (candidateImpl.saveCandidate(candidate)) {
                return "redirect:/login";
            }
        }
        return "redirect:/login";
    }

    @GetMapping(value = "/profile")
    public String candidateProfile(Authentication authentication, Model model) {
        String username = authentication.getName();
        User user = userImpl.findUserByUsername(username);
        Candidate candidate = candidateImpl.findByUser(user);
        model.addAttribute("user", user);
        model.addAttribute("candidate", candidate);
        model.addAttribute("listLanguages", languageImpl.listLanguages());
        model.addAttribute("listCourses", courseImpl.findByIdcandidate(candidate));
        model.addAttribute("listStudies", studyImpl.findByIdcandidate(candidate));
        model.addAttribute("listExperiences", experienceImpl.findByCandidate(candidate));
        model.addAttribute("listCertifications", certificationImpl.findByCandidate(candidate));
        model.addAttribute("listSkills", skillImpl.findByCandidate(candidate));
        return "users/profiles/profileCandidate";
    }
    //Listado de Contactos
    @GetMapping(value="/listContacts")
    public String listContacts(Model model) {
        model.addAttribute("listContactos", userImpl.listUsers());
        return "index";
    }

    //Listado de candidatos (Reclutador)
    @GetMapping(value="/listCandidates")
    public String listCandidates(Model model) {
        model.addAttribute("listContactos", userImpl.listUsers());
        return "";
    }

    //Listado de Reclutadores (Admin)
    @GetMapping(value="/listRecruiters")
    public String listRecruiters(Model model) {
        model.addAttribute("listContactos", userImpl.listUsers());
        return "";
    }

    //Perfil propio de candidato
    @GetMapping(value="/profile/{username}")
    public String profileUser(@PathVariable String username, Model model) {
        User user = userImpl.findUserByUsername(username);
        if (user != null) {
            model.addAttribute("user", user);
            return "";
        }
        return "";
    }

    //Perfil de candidato visto como reclutador
    @GetMapping(value="/candidate/{username}")
    public String profileVacant(@PathVariable String username, Model model) {
        User user = userImpl.findUserByUsername(username);
        if (user != null) {
            model.addAttribute("user", user);
            return "";
        }
        return "";
    }
}