package mx.edu.utez.poem.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.edu.utez.poem.Entity.Skill;
import mx.edu.utez.poem.ServiceImpl.SkillServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequestMapping("/poem")
public class SkillController {
    
    @Autowired
    SkillServiceImpl skillImpl;

    @PostMapping(value="/saveSkill")
    public String saveSkill(Skill skill) {
        if (skillImpl.saveSkill(skill)) {
            return "redirect:/poem/profile";
        }
        return "redirect:/poem/profile";
    }
}