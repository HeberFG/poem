package mx.edu.utez.poem.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.edu.utez.poem.Entity.Study;
import mx.edu.utez.poem.ServiceImpl.StudyServiceImpl;

import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequestMapping("/poem")
public class StudyController {

    @Autowired
    StudyServiceImpl studyImpl;

    @PostMapping(value="/saveStudy")
    public String saveStudy(Study study) {
        if (studyImpl.saveStudy(study)) {
            return "redirect:/poem/profile";
        }
        return "redirect:/poem/profile";
    }
}