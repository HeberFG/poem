package mx.edu.utez.poem.Controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mx.edu.utez.poem.Entity.Certification;
import mx.edu.utez.poem.ServiceImpl.CandidateServiceImpl;
import mx.edu.utez.poem.ServiceImpl.CertificationServiceImpl;

import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequestMapping("/poem")
public class CertificationController {
    
    @Autowired
    CertificationServiceImpl certificationImpl;

    @Autowired
    CandidateServiceImpl candidateImpl;

    @PostMapping(value="/saveCertification")
    public String saveCertification(Certification certification, @RequestParam("idcandidate") Long idcandidate, @RequestParam("fechaObtenida") String fechaObtenida, @RequestParam("fechaExpira") String fechaExpira) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        certification.setObtainingdate(format.parse(fechaObtenida));
        certification.setExpiredate(format.parse(fechaExpira));
        certification.setCandidate(candidateImpl.findCandidateById(idcandidate));
        System.out.println(idcandidate);
        if (certificationImpl.saveCertification(certification)) {
            return "redirect:/poem/profile";
        }
        return "redirect:/poem/profile";
    }
}