package mx.edu.utez.poem.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.edu.utez.poem.Entity.Course;
import mx.edu.utez.poem.ServiceImpl.CourseServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;



@Controller
@RequestMapping("/poem")
public class CourseController {
    
    @Autowired
    CourseServiceImpl courseImpl;

    @PostMapping(value="/saveCourse")
    public String saveCourse(Course course) {
        if (courseImpl.saveCours(course)) {
            return "redirect:/poem/profile";
        }
        return "redirect:/";
    }
}
